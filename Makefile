DOKUMENT = main

all: $(DOKUMENT).pdf

$(DOKUMENT).pdf: $(DOKUMENT).tex
	pdflatex $(DOKUMENT).tex

clean:
	rm $(DOKUMENT).aux $(DOKUMENT).log \
	$(DOKUMENT).toc $(DOKUMENT).bbl \
	$(DOKUMENT).idx $(DOKUMENT).ilg \
	$(DOKUMENT).ind $(DOKUMENT).dvi \
	$(DOKUMENT).pdf $(DOKUMENT).ps \
	$(DOKUMENT).blg
